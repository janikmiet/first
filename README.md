# Setting up RStudio environment

This project helps you to setup your personal data science environment.


## Install a Custom .Rprofile

```
download.file(url = "https://gitlab.com/janikmiet/first/-/raw/master/data/.Rprofile", destfile = "~/.Rprofile")
```
Restart R and get help by command 

```
rprofile_functions()
```

## Install 130+ Most Common R-packages

After installing a custom .Rprofile, Install R packages by function:

```
install.packages.list()
```

or without Custom .Rprofile:

```
PACKAGES <- scan("https://gitlab.com/janikmiet/first/-/raw/master/data/r_library_list.txt", what = "character")
inst <- match(PACKAGES, .packages(all=TRUE))
need <- which(is.na(inst))
if (length(need) > 0) install.packages(PACKAGES[need], 
                                       dependencies=T)
```

